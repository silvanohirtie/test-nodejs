import { connect } from 'mongoose'
import bodyParser from 'body-parser'
import express from 'express'
import UserModel from './UserModel'
import cors from 'cors'
import path from 'path'

connect('mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo', { useNewUrlParser: true, useUnifiedTopology: true  })

const app = express()
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//Nella homepage verremo reindirizzati verso index.html
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname + '/index.html'))
})

//Andando sulla pagina /users/ riceveremo le informazioni in formato JSON sugli utenti nel database
app.get('/users', (req, res) => {
  UserModel.find((err, results) => {
    res.send(results)
  })
})

//Mandando una richiesta di POST a /users potremmo inserire nel database un nuovo utente
app.post('/users', (req, res) => {

  let user = new UserModel()
  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password

  user.save((err, newUser) => {
    if (err) res.send(err)
    else res.send(newUser)
  })
})

//Eliminiamo un valore nel database tramite una richiesta di DELETE, :id indica l'id mongo che andremo ad eliminare
//Useremo UserModel, overo uno schema di mongo che indica come devono essere inseriti i dati.
app.delete('/delete/:id', (req, res) => {
  let query = {_id:req.params.id}
  UserModel.remove(query, function(err) {
    if (err) {
      console.log(err)
    }
    else {
      res.send("Deleted!")
    }
});
})

//Se viene inviata una richiesta di POST con un id mongo, collezioneremo le infomazioni della richiesta di post (fatta tramite AJAX)
//in una variabile, che poi sarà il nostro $set per inserire e modificare le infomazioni all'inteno del database
app.post('/edit/:id', (req, res) => {
  let query = {_id:req.params.id}
  let newInfo = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    password: req.body.password
  }
  UserModel.updateOne(query, {$set: newInfo}, function(err, result) {
    console.log('Updated!')
  })
})


app.listen(8080, () => console.log('Example app listening on port 8080!'))
